# Export Plugin
module.exports = (BasePlugin) ->
    # Requires
    Twitter = require 'twitter'
    TwitterText = require 'twitter-text'
    _ = require 'lodash'
    Path = require 'path'
    Fs = require 'fs-extra'
    moment = require 'moment'

    # Create Twitter Client
    Client = new Twitter({
        consumer_key: process.env.TWITTER_CONSUMER_KEY,
        consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
        access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
        access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
        request_options:
            proxy: process.env.TWITTER_PROXY or false
    })

    # Define Plugin
    class twitterPlugin extends BasePlugin
        # Plugin name
        name: 'twitter'

        # Default config.  Can be overridden in plugin settings
        config:
            extension: ".json"
            injectDocumentHelper: null
            relativeDirPath: "twitter"

        # Get Rate Limits
        getRateLimitStatus: (cb) ->
            # Prepare
            docpad = @docpad
            path = 'application/rate_limit_status'
            config = @getConfig()

            Client.get path, (err, status, response) ->
                docpad.log 'warn', 'Twitter Response Headers from', path, response.headers unless status.resources
                docpad.log 'error', err if err
                cb null, status # Return the tweets

        # Get Tweets
        getTweets: (feedConfig, cb) ->
            # Skip if rate limit is exceeded
            return cb null, [] if feedConfig.rateExceeded

            # Prepare
            docpad = @docpad
            path = feedConfig.path
            params = feedConfig.params

            Client.get path, params, (err, tweets, response) ->
                docpad.log 'warn', 'Twitter Response Headers from', path, response.headers unless tweets.length
                docpad.log 'error', err if err
                cb null, tweets # Return the tweets

        createTweetDocuments: (tweets, feedConfig, next) ->
            # Prepare
            plugin = @
            docpad = @docpad

            total = tweets.length
            processed = 0

            for tweet in tweets
                do (tweet) ->
                    # docpad.log 'info', 'processing tweet', tweet.id
                    plugin.createDocument tweet, feedConfig, (err, result) ->
                        return next err if err
                        processed++

                        return next err, result if processed is total

        createDocument: (tweet, feedConfig, next) ->
            # Prepare
            plugin = @
            docpad = @docpad
            config = @getConfig()

            # Extract
            {id, created_at} = tweet

            # Prepare dates
            created = new Date(created_at)
            tweetDateReadable = moment(created).format('YYYY-MM-DD HH:mm:ss')

            # Fetch
            document = docpad.getFile({id: id})
            documentTime = document?.get('mtime') or null
            relativePath = "twitter/#{feedConfig.path}/#{id}#{config.extension}"
            documentPath = "twitter/#{feedConfig.path}/#{tweetDateReadable}-#{id}#{config.extension}"

            # Compare
            if documentTime and documentTime.toString() is created.toString()
                # Skip
                return next null, null

            # Add Page Metadata
            meta =
                id: id
                ctime: created
                mtime: created
                created_timestamp: created.getTime()
                date: tweetDateReadable
                feedPath: feedConfig.path
                relativePath: documentPath
                tweet: tweet
                standalone: true
                referencesOthers: false

            # Prepare
            tweetData = JSON.stringify(tweet, null, '\t')
            documentAttributes =
                data: tweetData
                meta: meta

            # Existing document
            if document?
                document.set(documentAttributes)

            # New Document
            else
                # Create document from opts
                document = docpad.createDocument(documentAttributes)

            # Need to set document.stat.size to avoid error in hapi-server.
            document.setStat({size: 0})

            # Inject document helper
            config.injectDocumentHelper?.call(plugin, document)

            # Write to file for fallback
            Fs.outputJsonSync Path.join(docpad.getConfig().outPath, documentPath), tweet, { spaces: 2 }

            # Load the document
            document.action 'load', (err) ->
                # Check
                return next err, document if err

                # Add it to the database (with b/c compat)
                docpad.addModel?(document) or docpad.getDatabase().add(document)

                # Complete
                return next null, document

            # Return the document
            # return document


        templateHelpers:
            parseTweet: (tweet, options) ->
                options ?= {}
                options.targetBlank ?= false
                imageMarkup = {}

                replaceMediaLink = (url, replacement) ->
                    replacement ?= ''
                    tweet.text = tweet.text.replace url, replacement

                parseMedia = (entity, index, placement) ->

                    switch placement
                        when 'inline'
                            {media_url, media_url_https, url, expanded_url} = entity
                            target = if options.targetBlank then '_blank' else '_self'
                            media_url = if options.ssl then media_url_https else media_url

                            tag = "^image-#{index}^"
                            markup = """
                                <a class="twitter-image-link" href="#{url}" target="#{target}" title="#{expanded_url}" rel="nofollow">
                                    <img class="twitter-image" alt="Embedded twitter photo" src="#{media_url}" />
                                </a>
                            """

                            imageMarkup[tag] = markup
                            replaceMediaLink entity.url, tag

                        when 'strip'
                            replaceMediaLink entity.url

                if options.media?
                    if tweet.entities?.media?.length
                        parseMedia entity, index, options.media for entity, index in tweet.entities.media

                markup = (TwitterText.autoLink tweet.text, options)

                for tag, replacement of imageMarkup
                    markup = markup.replace tag, replacement

                return markup

        # Extend Template Data
        extendTemplateData: ({templateData}) ->
            templateData.twitter or= {}

            # Inject template helpers into template data
            for own templateHelperName, templateHelper of @templateHelpers
                templateData.twitter[templateHelperName] = templateHelper

            #Chain
            @


        # Extend Collections
        # Create our live collection for our menus
        extendCollections: ->
            # Prepare
            config = @getConfig()
            docpad = @docpad

            {feeds} = config

            _.forEach feeds, (feed, key) ->

                # Check to make sure the required properties are present
                if feed.collectionName
                    # Create the collection
                    collectionPath = [config.relativeDirPath, feed.path].join('/')
                    collection = docpad.getFiles({relativeDirPath: $startsWith: collectionPath}, [date:-1])
                    # Set the collection
                    docpad.setCollection(feed.collectionName, collection)

            # Chain
            @


        # Populate Collections
        # Import Drupal Pages into the Database
        populateCollections: ({templateData}, next) ->
            # Prepare
            plugin = @
            config = @getConfig()
            docpad = @docpad
            database = docpad.getDatabase()

            # Extend the template data
            templateData.twitter or= {}
            totalFeeds = _.keysIn(config.feeds).length
            processedFeeds = 0
            tweetData = []
            rateLimits = {}

            # First lets get the Rate limit statuses, so we don't try to pull from endpoints for which we've exceeded the rate limits.
            plugin.getRateLimitStatus (err, status) ->
                docpad.log 'warn', err if err
                rateLimits = status.resources

                # Inject template helpers into template data
                for own name, feedConfig of config.feeds
                    docpad.log 'info', 'Twitter API checking feed for:', feedConfig.path
                    try
                        # check rate limiit
                        rateLimitKey = feedConfig?.path?.split('/')[0]
                        rateLimitPath = '/' + feedConfig?.path
                        rateRemaining = parseInt(rateLimits?[rateLimitKey]?[rateLimitPath]?.remaining, 10)

                        feedConfig.rateExceeded = rateRemaining is 0
                        # feedConfig.rateExceeded = true # Used to test rebuilding from files backups

                        docpad.log 'info', "Twitter API remaining requests for #{rateLimitPath}: #{rateRemaining} "
                    catch err
                        docpad.log 'error', err

                    plugin.getTweets feedConfig, (err, results) ->
                        docpad.log 'warn', err if err
                        processedFeeds++

                        templateData.twitter[name] = []
                        collectionPath = [docpad.getConfig().outPath, config.relativeDirPath, feedConfig.path].join('/')

                        # Ensure the collection path exists
                        Fs.ensureDirSync(collectionPath)

                        if !results.length
                            docpad.log 'info', 'Getting tweets from cache.'
                            # Load our stored tweets as a fallback in case we can't hit the twitter api due to rate limits
                            files = Fs.readdirSync(collectionPath)

                            files.forEach (file, index) ->
                                filePath = [collectionPath, file].join('/')

                                templateData.twitter[name].push Fs.readJSONSync(filePath)

                                if index is files.length - 1
                                    tweetData.push templateData.twitter[name]

                        else
                            templateData.twitter[name] = results
                            tweetData.push results

                            # Since we have results, let's empty the current files
                            Fs.removeSync collectionPath

                        if processedFeeds is totalFeeds
                            totalTweetCollections = tweetData.length
                            processedTweetCollections = 0

                            return next() if processedTweetCollections is totalTweetCollections

                            for tweets in tweetData
                                do (tweets) ->
                                    plugin.createTweetDocuments tweets, feedConfig, (err, result) ->
                                        return next err if err
                                        processedTweetCollections++

                                        return next() if processedTweetCollections is totalTweetCollections

            # Chain
            @
