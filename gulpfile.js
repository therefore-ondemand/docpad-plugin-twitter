var gulp = require('gulp');

// gulp util

// gulp support
var debug = require('gulp-debug');
var coffee = require('gulp-coffee');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var watch = require('gulp-watch');


/**
 * compile custom compile coffee
 */
gulp.task('coffee', function() {
	return gulp.src('./src/*.coffee')
		.pipe(coffee())
		.pipe(rename(function(path) {
			path.basename = path.basename.replace('.coffee', '.js');
			// path.dirname = path.dirname.replace('src', 'out');
		}))
		.pipe(debug({ title: 'coffee' }))
		.pipe(gulp.dest('./out'));
});

gulp.task('watch', function() {
	gulp.watch('./src/*.coffee', ['coffee']);
});

gulp.task('default', ['coffee']);