(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  module.exports = function(BasePlugin) {
    var Client, Fs, Path, Twitter, TwitterText, _, moment, twitterPlugin;
    Twitter = require('twitter');
    TwitterText = require('twitter-text');
    _ = require('lodash');
    Path = require('path');
    Fs = require('fs-extra');
    moment = require('moment');
    Client = new Twitter({
      consumer_key: process.env.TWITTER_CONSUMER_KEY,
      consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
      access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
      access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
      request_options: {
        proxy: process.env.TWITTER_PROXY || false
      }
    });
    return twitterPlugin = (function(superClass) {
      extend(twitterPlugin, superClass);

      function twitterPlugin() {
        return twitterPlugin.__super__.constructor.apply(this, arguments);
      }

      twitterPlugin.prototype.name = 'twitter';

      twitterPlugin.prototype.config = {
        extension: ".json",
        injectDocumentHelper: null,
        relativeDirPath: "twitter"
      };

      twitterPlugin.prototype.getRateLimitStatus = function(cb) {
        var config, docpad, path;
        docpad = this.docpad;
        path = 'application/rate_limit_status';
        config = this.getConfig();
        return Client.get(path, function(err, status, response) {
          if (!status.resources) {
            docpad.log('warn', 'Twitter Response Headers from', path, response.headers);
          }
          if (err) {
            docpad.log('error', err);
          }
          return cb(null, status);
        });
      };

      twitterPlugin.prototype.getTweets = function(feedConfig, cb) {
        var docpad, params, path;
        if (feedConfig.rateExceeded) {
          return cb(null, []);
        }
        docpad = this.docpad;
        path = feedConfig.path;
        params = feedConfig.params;
        return Client.get(path, params, function(err, tweets, response) {
          if (!tweets.length) {
            docpad.log('warn', 'Twitter Response Headers from', path, response.headers);
          }
          if (err) {
            docpad.log('error', err);
          }
          return cb(null, tweets);
        });
      };

      twitterPlugin.prototype.createTweetDocuments = function(tweets, feedConfig, next) {
        var docpad, i, len, plugin, processed, results1, total, tweet;
        plugin = this;
        docpad = this.docpad;
        total = tweets.length;
        processed = 0;
        results1 = [];
        for (i = 0, len = tweets.length; i < len; i++) {
          tweet = tweets[i];
          results1.push((function(tweet) {
            return plugin.createDocument(tweet, feedConfig, function(err, result) {
              if (err) {
                return next(err);
              }
              processed++;
              if (processed === total) {
                return next(err, result);
              }
            });
          })(tweet));
        }
        return results1;
      };

      twitterPlugin.prototype.createDocument = function(tweet, feedConfig, next) {
        var config, created, created_at, docpad, document, documentAttributes, documentPath, documentTime, id, meta, plugin, ref, relativePath, tweetData, tweetDateReadable;
        plugin = this;
        docpad = this.docpad;
        config = this.getConfig();
        id = tweet.id, created_at = tweet.created_at;
        created = new Date(created_at);
        tweetDateReadable = moment(created).format('YYYY-MM-DD HH:mm:ss');
        document = docpad.getFile({
          id: id
        });
        documentTime = (document != null ? document.get('mtime') : void 0) || null;
        relativePath = "twitter/" + feedConfig.path + "/" + id + config.extension;
        documentPath = "twitter/" + feedConfig.path + "/" + tweetDateReadable + "-" + id + config.extension;
        if (documentTime && documentTime.toString() === created.toString()) {
          return next(null, null);
        }
        meta = {
          id: id,
          ctime: created,
          mtime: created,
          created_timestamp: created.getTime(),
          date: tweetDateReadable,
          feedPath: feedConfig.path,
          relativePath: documentPath,
          tweet: tweet,
          standalone: true,
          referencesOthers: false
        };
        tweetData = JSON.stringify(tweet, null, '\t');
        documentAttributes = {
          data: tweetData,
          meta: meta
        };
        if (document != null) {
          document.set(documentAttributes);
        } else {
          document = docpad.createDocument(documentAttributes);
        }
        document.setStat({
          size: 0
        });
        if ((ref = config.injectDocumentHelper) != null) {
          ref.call(plugin, document);
        }
        Fs.outputJsonSync(Path.join(docpad.getConfig().outPath, documentPath), tweet, {
          spaces: 2
        });
        return document.action('load', function(err) {
          if (err) {
            return next(err, document);
          }
          (typeof docpad.addModel === "function" ? docpad.addModel(document) : void 0) || docpad.getDatabase().add(document);
          return next(null, document);
        });
      };

      twitterPlugin.prototype.templateHelpers = {
        parseTweet: function(tweet, options) {
          var entity, i, imageMarkup, index, len, markup, parseMedia, ref, ref1, ref2, replaceMediaLink, replacement, tag;
          if (options == null) {
            options = {};
          }
          if (options.targetBlank == null) {
            options.targetBlank = false;
          }
          imageMarkup = {};
          replaceMediaLink = function(url, replacement) {
            if (replacement == null) {
              replacement = '';
            }
            return tweet.text = tweet.text.replace(url, replacement);
          };
          parseMedia = function(entity, index, placement) {
            var expanded_url, markup, media_url, media_url_https, tag, target, url;
            switch (placement) {
              case 'inline':
                media_url = entity.media_url, media_url_https = entity.media_url_https, url = entity.url, expanded_url = entity.expanded_url;
                target = options.targetBlank ? '_blank' : '_self';
                media_url = options.ssl ? media_url_https : media_url;
                tag = "^image-" + index + "^";
                markup = "<a class=\"twitter-image-link\" href=\"" + url + "\" target=\"" + target + "\" title=\"" + expanded_url + "\" rel=\"nofollow\">\n    <img class=\"twitter-image\" alt=\"Embedded twitter photo\" src=\"" + media_url + "\" />\n</a>";
                imageMarkup[tag] = markup;
                return replaceMediaLink(entity.url, tag);
              case 'strip':
                return replaceMediaLink(entity.url);
            }
          };
          if (options.media != null) {
            if ((ref = tweet.entities) != null ? (ref1 = ref.media) != null ? ref1.length : void 0 : void 0) {
              ref2 = tweet.entities.media;
              for (index = i = 0, len = ref2.length; i < len; index = ++i) {
                entity = ref2[index];
                parseMedia(entity, index, options.media);
              }
            }
          }
          markup = TwitterText.autoLink(tweet.text, options);
          for (tag in imageMarkup) {
            replacement = imageMarkup[tag];
            markup = markup.replace(tag, replacement);
          }
          return markup;
        }
      };

      twitterPlugin.prototype.extendTemplateData = function(arg) {
        var ref, templateData, templateHelper, templateHelperName;
        templateData = arg.templateData;
        templateData.twitter || (templateData.twitter = {});
        ref = this.templateHelpers;
        for (templateHelperName in ref) {
          if (!hasProp.call(ref, templateHelperName)) continue;
          templateHelper = ref[templateHelperName];
          templateData.twitter[templateHelperName] = templateHelper;
        }
        return this;
      };

      twitterPlugin.prototype.extendCollections = function() {
        var config, docpad, feeds;
        config = this.getConfig();
        docpad = this.docpad;
        feeds = config.feeds;
        _.forEach(feeds, function(feed, key) {
          var collection, collectionPath;
          if (feed.collectionName) {
            collectionPath = [config.relativeDirPath, feed.path].join('/');
            collection = docpad.getFiles({
              relativeDirPath: {
                $startsWith: collectionPath
              }
            }, [
              {
                date: -1
              }
            ]);
            return docpad.setCollection(feed.collectionName, collection);
          }
        });
        return this;
      };

      twitterPlugin.prototype.populateCollections = function(arg, next) {
        var config, database, docpad, plugin, processedFeeds, rateLimits, templateData, totalFeeds, tweetData;
        templateData = arg.templateData;
        plugin = this;
        config = this.getConfig();
        docpad = this.docpad;
        database = docpad.getDatabase();
        templateData.twitter || (templateData.twitter = {});
        totalFeeds = _.keysIn(config.feeds).length;
        processedFeeds = 0;
        tweetData = [];
        rateLimits = {};
        plugin.getRateLimitStatus(function(err, status) {
          var feedConfig, name, rateLimitKey, rateLimitPath, rateRemaining, ref, ref1, ref2, ref3, results1;
          if (err) {
            docpad.log('warn', err);
          }
          rateLimits = status.resources;
          ref = config.feeds;
          results1 = [];
          for (name in ref) {
            if (!hasProp.call(ref, name)) continue;
            feedConfig = ref[name];
            docpad.log('info', 'Twitter API checking feed for:', feedConfig.path);
            try {
              rateLimitKey = feedConfig != null ? (ref1 = feedConfig.path) != null ? ref1.split('/')[0] : void 0 : void 0;
              rateLimitPath = '/' + (feedConfig != null ? feedConfig.path : void 0);
              rateRemaining = parseInt(rateLimits != null ? (ref2 = rateLimits[rateLimitKey]) != null ? (ref3 = ref2[rateLimitPath]) != null ? ref3.remaining : void 0 : void 0 : void 0, 10);
              feedConfig.rateExceeded = rateRemaining === 0;
              docpad.log('info', "Twitter API remaining requests for " + rateLimitPath + ": " + rateRemaining + " ");
            } catch (_error) {
              err = _error;
              docpad.log('error', err);
            }
            results1.push(plugin.getTweets(feedConfig, function(err, results) {
              var collectionPath, files, i, len, processedTweetCollections, results2, totalTweetCollections, tweets;
              if (err) {
                docpad.log('warn', err);
              }
              processedFeeds++;
              templateData.twitter[name] = [];
              collectionPath = [docpad.getConfig().outPath, config.relativeDirPath, feedConfig.path].join('/');
              Fs.ensureDirSync(collectionPath);
              if (!results.length) {
                docpad.log('info', 'Getting tweets from cache.');
                files = Fs.readdirSync(collectionPath);
                files.forEach(function(file, index) {
                  var filePath;
                  filePath = [collectionPath, file].join('/');
                  templateData.twitter[name].push(Fs.readJSONSync(filePath));
                  if (index === files.length - 1) {
                    return tweetData.push(templateData.twitter[name]);
                  }
                });
              } else {
                templateData.twitter[name] = results;
                tweetData.push(results);
                Fs.removeSync(collectionPath);
              }
              if (processedFeeds === totalFeeds) {
                totalTweetCollections = tweetData.length;
                processedTweetCollections = 0;
                if (processedTweetCollections === totalTweetCollections) {
                  return next();
                }
                results2 = [];
                for (i = 0, len = tweetData.length; i < len; i++) {
                  tweets = tweetData[i];
                  results2.push((function(tweets) {
                    return plugin.createTweetDocuments(tweets, feedConfig, function(err, result) {
                      if (err) {
                        return next(err);
                      }
                      processedTweetCollections++;
                      if (processedTweetCollections === totalTweetCollections) {
                        return next();
                      }
                    });
                  })(tweets));
                }
                return results2;
              }
            }));
          }
          return results1;
        });
        return this;
      };

      return twitterPlugin;

    })(BasePlugin);
  };

}).call(this);
